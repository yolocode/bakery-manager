package models

import "testing"

func TestInitStep(t *testing.T) {
    name := "Etape 1"
	description := "Etape description"
    duration := 10
    rank := 1
	var s Step

	s.Init(name, description, duration, rank)

    if s.Name != name {
        t.Errorf("Step.Name = \"%s\"; want \"%s\"", s.Name, name)
    }

	if s.Description != description {
        t.Errorf("Step.Description = \"%s\"; want \"%s\"", s.Description, description)
    }

    if s.Duration != duration {
        t.Errorf("Step.Duration = %d; want %d", s.Duration, duration)
    }

	if s.Rank != rank {
        t.Errorf("Step.Rank = %d; want %d", s.Rank, rank)
    }
}