package models

type Recipe struct {
	Id          string
	Name        string
	Description string
	Steps       []Step
}

func (r *Recipe) Init(name string, description string) {
	r.Name = name
	r.Description = description
}

func (r *Recipe) AddStep(s Step) {
	r.Steps = append(r.Steps, s)
}

func (r *Recipe) RemoveStep(idx int) {
	r.Steps = append(r.Steps[:idx], r.Steps[(idx+1):]...)
}
