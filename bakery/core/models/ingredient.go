package models

import "fmt"

type IngredientUnityEnum string

const (
	IngredientUnityMG    IngredientUnityEnum = "mg"
	IngredientUnityG     IngredientUnityEnum = "g"
	IngredientUnityKG    IngredientUnityEnum = "kg"
	IngredientUnityML    IngredientUnityEnum = "ml"
	IngredientUnityL     IngredientUnityEnum = "l"
	IngredientUnityPinch IngredientUnityEnum = "pinch"
)

type Ingredient struct {
	Id       string
	Product  Product
	Quantity int
	Unity    IngredientUnityEnum
}

func (i *Ingredient) Init(quantity int, unity IngredientUnityEnum, product Product) {
	i.Quantity = quantity
	i.Unity = unity
	i.Product = product
}

func (i Ingredient) Print() {
	fmt.Printf("- Id : \"%s\", Quantity : \"%d\", Product : \"%s\", Unity : \"%s\"", i.Id, i.Quantity, i.Product.Id, i.Unity)
	fmt.Println()
}
