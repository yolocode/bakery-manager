package models

import "fmt"

type Product struct {
	Id          string
	Name        string
	Description string
}

func (p *Product) Init(name string, description string) {
	p.Name = name
	p.Description = description
}

func (p Product) Print() {
	fmt.Printf("- Id : \"%s\", Nom : \"%s\" , Description : \"%s\"", p.Id, p.Name, p.Description)
	fmt.Println()
}
