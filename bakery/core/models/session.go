package models

type Session struct {
	Id      string
	Recipes []Recipe
}

func (s *Session) Init() {
}

func (s *Session) AddRecipe(r Recipe) {
	s.Recipes = append(s.Recipes, r)
}

func (s *Session) RemoveRecipe(idx int) {
	s.Recipes = append(s.Recipes[:idx], s.Recipes[(idx+1):]...)
}
