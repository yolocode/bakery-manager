package models

import "testing"

func TestInitIngredient(t *testing.T) {
	// variables for ingredient
	quantity := 100
	unity := IngredientUnityMG
	var i Ingredient

	// variables for product in ingredient
	name := "Product"
	description := "Product description"
	var p Product
	p.Init(name, description)

	i.Init(quantity, unity, p)

	if i.Quantity != quantity {
		t.Errorf("i.Quantity = %d; want %d", i.Quantity, quantity)
	}

	if i.Unity != unity {
		t.Errorf("i.Unity = \"%s\"; want \"%s\"", i.Unity, "mg")
	}

	if i.Product.Name != name {
		t.Errorf("i.Product.Name  = \"%s\"; want \"%s\"", i.Product.Name, name)
	}
}
