package models

import (
	"testing"
)

func TestInitSession(t *testing.T) {
	var s Session

	s.Init()

	if len(s.Recipes) != 0 {
		t.Error("There must not be any recipe in this session")
	}
}

func TestAddRecipe(t *testing.T) {
	var r Recipe
	var s Session

	s.Init()
	r.Init("Recipe step", "Recipe step description")

	s.AddRecipe(r)

	if len(s.Recipes) != 1 {
		t.Error("There must be 1 recipe in this session")
	}
}

func TestRemoveRecipe(t *testing.T) {
	var r Recipe
	var s Session

	s.Init()
	r.Init("Recipe step", "Recipe step description")

	s.AddRecipe(r)

	if len(s.Recipes) != 1 {
		t.Error("There must be 1 recipe in this session")
	}

	s.RemoveRecipe(0)

	if len(s.Recipes) != 0 {
		t.Error("There must no be any recipe in this session")
	}
}
