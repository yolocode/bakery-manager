package models

import "testing"

func TestInitProduct(t *testing.T) {
    name := "Product"
	description := "Product description"
	var p Product

	p.Init(name, description)

    if p.Name != name {
        t.Errorf("p.Name = \"%s\"; want \"%s\"", p.Name, name)
    }

	if p.Description != description {
        t.Errorf("p.Description = \"%s\"; want \"%s\"", p.Description, description)
    }
}