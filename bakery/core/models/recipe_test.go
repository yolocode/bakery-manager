package models

import "testing"

func TestInitRecipe(t *testing.T) {
	name := "Pain blanc"
	description := "Pain blanc description"
	var r Recipe

	r.Init(name, description)

	if r.Name != name {
		t.Errorf("Recipe.Name = \"%s\"; want \"%s\"", r.Name, name)
	}

	if r.Description != description {
		t.Errorf("Recipe.Description = \"%s\"; want \"%s\"", r.Description, description)
	}
}

func TestAddStep(t *testing.T) {
	var r Recipe
	var s Step

	r.Init("Recipe step", "Recipe step description")

	if len(r.Steps) != 0 {
		t.Error("There must no be any step in this recipe")
	}

	s.Init("step", "step description", 10, 1)
	r.AddStep(s)

	if len(r.Steps) != 1 {
		t.Error("There must be only 1 step in this recipe")
	}

	if r.Steps[0].Name != "step" {
		t.Errorf("Recipe.Steps[0] = \"%s\"; want \"%s\"", r.Steps[0].Name, "step")
	}
}

func TestRemoveStep(t *testing.T) {
	var r Recipe
	var s Step

	r.Init("Recipe step", "Recipe step description")

	s.Init("step", "step description", 10, 1)
	r.AddStep(s)

	if len(r.Steps) != 1 {
		t.Error("There must be 1 step in this recipe")
	}

	r.RemoveStep(0)

	if len(r.Steps) != 0 {
		t.Error("There must no be any step in this recipe")
	}
}
