package models

type Step struct {
	Id          string
	Name        string
	Description string
	Duration    int
	Rank        int
}

func (s *Step) Init(name string, description string, duration int, rank int) {
	s.Name = name
	s.Description = description
	s.Duration = duration
	s.Rank = rank
}
