package ports

import (
	"bakery-manager/bakery/core/models"
)

type SessionRepository interface {
	Create(session *models.Session) error
	Update(session *models.Session) error
	Delete(id string) error
	Get(id string) (*models.Session, error)
	GetAll() ([]*models.Session, error)
}

type SessionService interface {
	Create(session *models.Session) error
	Update(session *models.Session) error
	Delete(id string) error
	Get(id string) (*models.Session, error)
	GetAll() ([]*models.Session, error)
}
