package ports

import (
	"bakery-manager/bakery/core/models"
)

type ProductRepository interface {
	Create(product *models.Product) error
	Update(product *models.Product) error
	Delete(id string) error
	Get(id string) (*models.Product, error)
	GetAll() ([]*models.Product, error)
}

type ProductService interface {
	Create(product *models.Product) error
	Update(product *models.Product) error
	Delete(id string) error
	Get(id string) (*models.Product, error)
	GetAll() ([]*models.Product, error)
}
