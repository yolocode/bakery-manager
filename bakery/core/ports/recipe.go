package ports

import (
	"bakery-manager/bakery/core/models"
)

type RecipeRepository interface {
	Create(recipe *models.Recipe) error
	Update(recipe *models.Recipe) error
	Delete(id string) error
	Get(id string) (*models.Recipe, error)
	GetAll() ([]*models.Recipe, error)
}

type RecipeService interface {
	Create(recipe *models.Recipe) error
	Update(recipe *models.Recipe) error
	Delete(id string) error
	Get(id string) (*models.Recipe, error)
	GetAll() ([]*models.Recipe, error)
}
