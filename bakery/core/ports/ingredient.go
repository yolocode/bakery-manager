package ports

import (
	"bakery-manager/bakery/core/models"
)

type IngredientRepository interface {
	Create(ingredient *models.Ingredient) error
	Update(ingredient *models.Ingredient) error
	Delete(id string) error
	Get(id string) (*models.Ingredient, error)
	GetAll() ([]*models.Ingredient, error)
}

type IngredientService interface {
	Create(ingredient *models.Ingredient) error
	Update(ingredient *models.Ingredient) error
	Delete(id string) error
	Get(id string) (*models.Ingredient, error)
	GetAll() ([]*models.Ingredient, error)
}
