package ports

import (
	"bakery-manager/bakery/core/models"
)

type StepRepository interface {
	Create(step *models.Step) error
	Update(step *models.Step) error
	Delete(id string) error
	Get(id string) (*models.Step, error)
	GetAll() ([]*models.Step, error)
}

type StepService interface {
	Create(step *models.Step) error
	Update(step *models.Step) error
	Delete(id string) error
	Get(id string) (*models.Step, error)
	GetAll() ([]*models.Step, error)
}
