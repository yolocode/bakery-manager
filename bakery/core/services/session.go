package services

import (
	"bakery-manager/bakery/core/models"
	"bakery-manager/bakery/core/ports"
)

type SessionService struct {
	repo ports.SessionRepository
}

func NewSessionService(repo ports.SessionRepository) *SessionService {
	return &SessionService{
		repo: repo,
	}
}

func (s *SessionService) Create(product *models.Session) error {
	return s.repo.Create(product)
}

func (s *SessionService) Update(product *models.Session) error {
	return s.repo.Update(product)
}

func (s *SessionService) Delete(id string) error {
	return s.repo.Delete(id)
}

func (s *SessionService) Get(id string) (*models.Session, error) {
	return s.repo.Get(id)
}

func (s *SessionService) GetAll() ([]*models.Session, error) {
	return s.repo.GetAll()
}
