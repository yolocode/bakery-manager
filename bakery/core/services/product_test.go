package services

import (
	"bakery-manager/adapters/repositories/daos"
	"bakery-manager/bakery/core/models"
	"fmt"
	"testing"
)

func TestProductService(t *testing.T) {
	daoFactory, _ := daos.GetDAOFactory("csv")
	repo := daoFactory.GetProductRepository()
	svc := NewProductService(repo)

	product := models.Product{Name: "test", Description: "test description"}

	svc.Create(&product)

	fmt.Printf("Product Id : %s", product.Id)
}
