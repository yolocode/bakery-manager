package services

import (
	"bakery-manager/bakery/core/models"
	"bakery-manager/bakery/core/ports"
)

type StepService struct {
	repo ports.StepRepository
}

func NewStepService(repo ports.StepRepository) *StepService {
	return &StepService{
		repo: repo,
	}
}

func (s *StepService) Create(product *models.Step) error {
	return s.repo.Create(product)
}

func (s *StepService) Update(product *models.Step) error {
	return s.repo.Update(product)
}

func (s *StepService) Delete(id string) error {
	return s.repo.Delete(id)
}

func (s *StepService) Get(id string) (*models.Step, error) {
	return s.repo.Get(id)
}

func (s *StepService) GetAll() ([]*models.Step, error) {
	return s.repo.GetAll()
}
