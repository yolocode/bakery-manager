package services

import (
	"bakery-manager/bakery/core/models"
	"bakery-manager/bakery/core/ports"
)

type RecipeService struct {
	repo ports.RecipeRepository
}

func NewRecipeService(repo ports.RecipeRepository) *RecipeService {
	return &RecipeService{
		repo: repo,
	}
}

func (r *RecipeService) Create(product *models.Recipe) error {
	return r.repo.Create(product)
}

func (r *RecipeService) Update(product *models.Recipe) error {
	return r.repo.Update(product)
}

func (r *RecipeService) Delete(id string) error {
	return r.repo.Delete(id)
}

func (r *RecipeService) Get(id string) (*models.Recipe, error) {
	return r.repo.Get(id)
}

func (r *RecipeService) GetAll() ([]*models.Recipe, error) {
	return r.repo.GetAll()
}
