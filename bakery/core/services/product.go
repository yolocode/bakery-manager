package services

import (
	"bakery-manager/bakery/core/models"
	"bakery-manager/bakery/core/ports"
)

type ProductService struct {
	repo ports.ProductRepository
}

func NewProductService(repo ports.ProductRepository) *ProductService {
	return &ProductService{
		repo: repo,
	}
}

func (p *ProductService) Create(product *models.Product) error {
	return p.repo.Create(product)
}

func (p *ProductService) Update(product *models.Product) error {
	return p.repo.Update(product)
}

func (p *ProductService) Delete(id string) error {
	return p.repo.Delete(id)
}

func (p *ProductService) Get(id string) (*models.Product, error) {
	return p.repo.Get(id)
}

func (p *ProductService) GetAll() ([]*models.Product, error) {
	return p.repo.GetAll()
}
