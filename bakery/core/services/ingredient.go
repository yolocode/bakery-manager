package services

import (
	"bakery-manager/bakery/core/models"
	"bakery-manager/bakery/core/ports"
)

type IngredientService struct {
	repo ports.IngredientRepository
}

func NewIngredientService(repo ports.IngredientRepository) *IngredientService {
	return &IngredientService{
		repo: repo,
	}
}

func (i *IngredientService) Create(ingredient *models.Ingredient) error {
	return i.repo.Create(ingredient)
}

func (i *IngredientService) Update(ingredient *models.Ingredient) error {
	return i.repo.Update(ingredient)
}

func (i *IngredientService) Delete(id string) error {
	return i.repo.Delete(id)
}

func (i *IngredientService) Get(id string) (*models.Ingredient, error) {
	return i.repo.Get(id)
}

func (i *IngredientService) GetAll() ([]*models.Ingredient, error) {
	return i.repo.GetAll()
}
