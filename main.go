package main

import (
	"bakery-manager/adapters/handlers/cmd"
	"bakery-manager/adapters/handlers/web"
	"log"
	"os"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	handlerType := os.Getenv("HANDLER")
	switch handlerType {
	case "cmd":
		cmdHandler := cmd.NewCMDHandler()
		cmdHandler.CallMenu()
	case "http":
		httpHandler := web.NewHTTPHandler()
		httpHandler.InitRoutes()
	}
}
