package cmd

import (
	"bakery-manager/adapters/repositories/daos"
	"bakery-manager/bakery/core/models"
	"bakery-manager/bakery/core/services"
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

type CMDHandler struct {
	svc services.ProductService
}

func NewCMDHandler() *CMDHandler {
	daoFactory, _ := daos.GetDAOFactory("csv")
	repo := daoFactory.GetProductRepository()
	svc := services.NewProductService(repo)
	return &CMDHandler{
		svc: (*svc),
	}
}

func handleActionChoice() int {
	var (
		scanner   = bufio.NewScanner(os.Stdin)
		err       error
		choice    int
		goodEntry = false
	)
	for goodEntry == false {
		scanner.Scan()
		choice, err = strconv.Atoi(scanner.Text())
		if err != nil { //vérifier si l'utilisateur a rentré un nombre
			fmt.Println("Entrez un nombre et non autre chose !")
		} else if choice < 1 || choice > 6 {
			fmt.Print("Entrez un choix valable")
		} else {
			goodEntry = true
		}
	}

	return choice
}

func (ch *CMDHandler) CallMenu() {
	leave := false
	fmt.Println("Bonjour, Bienvenue dans l'application qui vous aidera à gérer votre boulangerie")
	for leave == false {

		fmt.Println("Que souhaitez-vous faire :")
		fmt.Println("1 - Créer un produit")
		fmt.Println("2 - Mettre à jour un produit")
		fmt.Println("3 - Supprimer un produit")
		fmt.Println("4 - Afficher un produit")
		fmt.Println("5 - Afficher tous les produits")
		fmt.Println("6 - Quitter")

		choice := handleActionChoice()

		switch choice {
		case 1:
			ch.CreateProduct()
		case 2:
			ch.UpdateProduct()
		case 3:
			ch.DeleteProduct()
		case 4:
			ch.GetProduct()
		case 5:
			ch.GetAllProduct()
		case 6:
			leave = true
		}
	}
}

func (ch *CMDHandler) CreateProduct() {
	var (
		scanner     = bufio.NewScanner(os.Stdin)
		name        string
		description string
		obj         models.Product
		wrightEntry = false
	)
	fmt.Println("Vous allez créer un produit :")
	for wrightEntry == false {
		fmt.Print("- Saisissez le nom du produit : ")
		scanner.Scan()
		name = scanner.Text()

		if name == "" {
			fmt.Println("Veuillez entrer un nom valable")
		} else {
			wrightEntry = true
		}
	}

	wrightEntry = false

	for wrightEntry == false {
		fmt.Print("- Saisissez la description du produit : ")
		scanner.Scan()
		description = scanner.Text()

		if description == "" {
			fmt.Println("Veuillez entrer une description valable")
		} else {
			wrightEntry = true
		}
	}

	obj.Init(name, description)

	ch.svc.Create(&obj)
}

func (ch *CMDHandler) UpdateProduct() {
	var (
		scanner        = bufio.NewScanner(os.Stdin)
		id             string
		product        *models.Product
		newName        string
		newDescription string
		err            error
	)
	fmt.Print("Saisissez l'identifiant du produit à mettre à jour :")
	scanner.Scan()
	id = scanner.Text()

	product, err = ch.svc.Get(id)
	if err != nil {
		fmt.Print(err)
		return
	}

	oldName := (*product).Name
	oldDescription := (*product).Description

	fmt.Printf("Saisissez le nouveau nom (\"%s\" par défaut): ", oldName)
	scanner.Scan()
	newName = scanner.Text()

	if newName != "" {
		(*product).Name = newName
	}

	fmt.Printf("Saisissez la nouvelle description (\"%s\" par défaut): ", oldDescription)
	scanner.Scan()
	newDescription = scanner.Text()

	if newDescription != "" {
		(*product).Description = newDescription
	}

	ch.svc.Update(product)

	fmt.Println("Produit mis à jour : ")
	(*product).Print()
	fmt.Println()
}

func (ch *CMDHandler) DeleteProduct() {
	var (
		scanner    = bufio.NewScanner(os.Stdin)
		idToDelete string
		product    *models.Product
		err        error
	)
	fmt.Print("Saisissez l'identifiant du produit à supprimer :")
	scanner.Scan()
	idToDelete = scanner.Text()

	product, err = ch.svc.Get(idToDelete)
	if err != nil {
		fmt.Print(err)
	}

	ch.svc.Delete(idToDelete)

	fmt.Println("Ce produit a été supprimé : ")
	(*product).Print()
}

func (ch *CMDHandler) GetProduct() {
	var (
		scanner = bufio.NewScanner(os.Stdin)
		id      string
		product *models.Product
		err     error
	)
	fmt.Print("Saisissez l'identifiant du produit à afficher :")
	scanner.Scan()
	id = scanner.Text()

	product, err = ch.svc.Get(id)
	if err != nil {
		fmt.Print(err)
	}

	(*product).Print()
}

func (ch *CMDHandler) GetAllProduct() {
	products, err := ch.svc.GetAll()
	if err != nil {
		log.Printf("GetAllProduct : %s", err)
	}

	for _, product := range products {
		product.Print()
	}
	fmt.Println()
}
