package api

import (
	"bakery-manager/adapters/handlers/web/binding"
	"bakery-manager/bakery/core/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RecipeHTTPHandler struct {
	rsvc services.RecipeService
	ssvc services.StepService
}

func (h *RecipeHTTPHandler) HandleRecipeRoutes(router *gin.Engine, rsvc services.RecipeService) {
	h.rsvc = rsvc
	router.GET("/recipe/", h.GetAll)
	router.POST("/recipe", h.Create)
	router.POST("/recipe/:id", h.Update)
	router.GET("/recipe/:id", h.Get)
	router.DELETE("/recipe/:id", h.Delete)
}

func (h *RecipeHTTPHandler) Get(ctx *gin.Context) {
	id := ctx.Param("id")
	product, err := h.rsvc.Get(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, &product)
}

func (h *RecipeHTTPHandler) GetAll(ctx *gin.Context) {
	products, err := h.rsvc.GetAll()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, &products)
}

func (h *RecipeHTTPHandler) Create(ctx *gin.Context) {
	var product binding.Recipe
	if err := ctx.ShouldBindJSON(&product); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
		})
		return
	}

	productToSave := product.ToModel()
	err := h.rsvc.Create(&productToSave)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "New Product created successfully",
	})
}

func (h *RecipeHTTPHandler) Update(ctx *gin.Context) {
	id := ctx.Param("id")
	var recipe binding.Recipe
	if err := ctx.ShouldBindJSON(&recipe); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
		})
		return
	}
	product, err := h.rsvc.Get(ingredientBinding.ProductId)

	recipeToUpdate := recipe.ToModel()
	recipeToUpdate.Id = id
	err := h.rsvc.Update(&recipeToUpdate)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "Product updated successfully",
	})
}

func (h *RecipeHTTPHandler) Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	err := h.rsvc.Delete(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "Product deleted successfully",
	})
}
