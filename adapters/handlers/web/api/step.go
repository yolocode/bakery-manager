package api

import (
	"bakery-manager/adapters/handlers/web/binding"
	"bakery-manager/bakery/core/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

type StepHTTPHandler struct {
	svc services.StepService
}

func (h *StepHTTPHandler) HandleStepRoutes(router *gin.Engine, svc services.StepService) {
	h.svc = svc
	router.GET("/step/", h.GetAll)
	router.POST("/step", h.Create)
	router.POST("/step/:id", h.Update)
	router.GET("/step/:id", h.Get)
	router.DELETE("/step/:id", h.Delete)
}

func (h *StepHTTPHandler) Get(ctx *gin.Context) {
	id := ctx.Param("id")
	product, err := h.svc.Get(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, &product)
}

func (h *StepHTTPHandler) GetAll(ctx *gin.Context) {
	products, err := h.svc.GetAll()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, &products)
}

func (h *StepHTTPHandler) Create(ctx *gin.Context) {
	var product binding.Product
	if err := ctx.ShouldBindJSON(&product); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
		})
		return
	}

	productToSave := product.ToModel()
	err := h.svc.Create(&productToSave)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "New Product created successfully",
	})
}

func (h *StepHTTPHandler) Update(ctx *gin.Context) {
	id := ctx.Param("id")
	var product binding.Product
	if err := ctx.ShouldBindJSON(&product); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
		})
		return
	}

	productToUpdate := product.ToModel()
	productToUpdate.Id = id
	err := h.svc.Update(&productToUpdate)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "Product updated successfully",
	})
}

func (h *StepHTTPHandler) Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	err := h.svc.Delete(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "Product deleted successfully",
	})
}
