package api

import (
	"bakery-manager/adapters/handlers/web/binding"
	"bakery-manager/bakery/core/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

type IngredientHTTPHandler struct {
	isvc services.IngredientService
	psvc services.ProductService
}

func (h *IngredientHTTPHandler) HandleIngredientRoutes(router *gin.Engine, isvc services.IngredientService, psvc services.ProductService) {
	h.isvc = isvc
	h.psvc = psvc
	router.GET("/ingredient/", h.GetAll)
	router.POST("/ingredient", h.Create)
	router.POST("/ingredient/:id", h.Update)
	router.GET("/ingredient/:id", h.Get)
	router.DELETE("/ingredient/:id", h.Delete)
}

func (h *IngredientHTTPHandler) Get(ctx *gin.Context) {
	id := ctx.Param("id")
	ingredient, err := h.isvc.Get(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, &ingredient)
}

func (h *IngredientHTTPHandler) GetAll(ctx *gin.Context) {
	ingredients, err := h.isvc.GetAll()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, &ingredients)
}

func (h *IngredientHTTPHandler) Create(ctx *gin.Context) {
	var ingredientBinding binding.Ingredient
	if err := ctx.ShouldBindJSON(&ingredientBinding); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
		return
	}

	product, err := h.psvc.Get(ingredientBinding.ProductId)
	if err == nil {
		ingredientToSave := ingredientBinding.ToModel(*product)
		err := h.isvc.Create(&ingredientToSave)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err,
			})
			return
		}
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "New ingredient created successfully",
	})
}

func (h *IngredientHTTPHandler) Update(ctx *gin.Context) {
	id := ctx.Param("id")
	var ingredientBinding binding.Ingredient
	if err := ctx.ShouldBindJSON(&ingredientBinding); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
		return
	}

	ingredient, err := h.isvc.Get(id)

	if ingredientBinding.ProductId != ingredient.Product.Id {
		product, err := h.psvc.Get(ingredientBinding.ProductId)
		if err == nil {
			(*ingredient).Product = *product
		}
	}

	if err == nil {
		err = h.isvc.Update(ingredient)
	}

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "Ingredient updated successfully",
	})
}

func (h *IngredientHTTPHandler) Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	err := h.isvc.Delete(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "Ingredient deleted successfully",
	})
}
