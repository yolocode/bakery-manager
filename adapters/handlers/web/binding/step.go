package binding

import "bakery-manager/bakery/core/models"

type Step struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Duration    int    `json:"duration"`
	Rank        int    `json:"rank"`
}

func (s Step) ToModel() models.Step {
	var obj models.Step
	obj.Id = s.Id
	obj.Name = s.Name
	obj.Description = s.Description
	obj.Rank = s.Rank

	return obj
}
