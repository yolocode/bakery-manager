package binding

import "bakery-manager/bakery/core/models"

type Product struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (p Product) ToModel() models.Product {
	return models.Product{Id: p.Id, Name: p.Name, Description: p.Description}
}
