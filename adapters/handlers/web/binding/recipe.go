package binding

import (
	"bakery-manager/bakery/core/models"
)

type Recipe struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Steps       string `json:"steps"`
}

func (r Recipe) ToModel(steps []Step) models.Recipe {
	var modelRecipe models.Recipe

	modelRecipe.Id = r.Id
	modelRecipe.Name = r.Name
	modelRecipe.Description = r.Description

	return modelRecipe
}
