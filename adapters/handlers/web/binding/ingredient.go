package binding

import "bakery-manager/bakery/core/models"

type Ingredient struct {
	Id        string                     `json:"id"`
	Quantity  int                        `json:"quantity"`
	ProductId string                     `json:"product_id"`
	Unity     models.IngredientUnityEnum `json:"unity"`
}

func (i Ingredient) ToModel(product models.Product) models.Ingredient {
	return models.Ingredient{
		Product:  product,
		Quantity: i.Quantity,
		Unity:    i.Unity,
	}
}
