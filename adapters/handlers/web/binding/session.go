package binding

import "bakery-manager/bakery/core/models"

type Session struct {
	Id      string `json:"id"`
	Recipes string `json:"recipes"`
}

func (p Session) ToModel(recipes []models.Recipe) models.Session {
	var obj models.Session
	obj.Id = p.Id
	obj.Recipes = recipes

	return obj
}
