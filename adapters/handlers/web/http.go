package web

import (
	"bakery-manager/adapters/handlers/web/api"
	"bakery-manager/adapters/repositories/daos"
	"bakery-manager/bakery/core/services"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
)

type HTTPHandler struct {
	productSvc    services.ProductService
	ingredientSvc services.IngredientService
}

func NewHTTPHandler() *HTTPHandler {
	daoFactory, _ := daos.GetDAOFactory(os.Getenv("STORAGE"))
	productRepo := daoFactory.GetProductRepository()
	productSvc := services.NewProductService(productRepo)

	ingredientRepo := daoFactory.GetIngredientRepository()
	ingredientSvc := services.NewIngredientService(ingredientRepo)
	return &HTTPHandler{
		productSvc:    (*productSvc),
		ingredientSvc: (*ingredientSvc),
	}
}

func (h *HTTPHandler) InitRoutes() {
	router := gin.Default()

	productHandler := api.ProductHTTPHandler{}
	productHandler.HandleProductRoutes(router, h.productSvc)

	ingredientHandler := api.IngredientHTTPHandler{}
	ingredientHandler.HandleIngredientRoutes(router, h.ingredientSvc, h.productSvc)

	router.Run(":5000")

	fmt.Println("Server running and listening on port 5000")
}
