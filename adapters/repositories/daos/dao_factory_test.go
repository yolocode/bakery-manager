package daos

import (
	"bakery-manager/bakery/core/models"
	"fmt"
	"os"
	"testing"
)

func TestGetCSVDAOFactory(t *testing.T) {
	os.Setenv("CSV_FILE", "/tmp/bakery")
	fmt.Printf("test")
	csvDaoFactory, err := GetDAOFactory("csv")
	fmt.Print("essai")

	if err != nil {
		t.Error(err)
	}
	productDao := csvDaoFactory.GetProductRepository()

	m := models.Product{}
	m.Init("Produit", "Description")

	err = productDao.Create(&m)
	if err != nil {
		t.Error(err)
	}

}

func TestGetJSONDAOFactory(t *testing.T) {
	os.Setenv("CSV_FILE", "/tmp/bakery")
	jsonDaoFactory, err := GetDAOFactory("json")

	if err != nil {
		t.Error(err)
	}

	productDao := jsonDaoFactory.GetProductRepository()

	m := models.Product{}
	m.Init("Produit", "Description")

	err = productDao.Create(&m)
	if err != nil {
		t.Error(err)
	}
}
