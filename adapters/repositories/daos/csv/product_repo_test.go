package csv

import (
	"bakery-manager/bakery/core/models"
	"log"
	"testing"

	"github.com/gocarina/gocsv"
	"github.com/spf13/afero"
)

func TestCreateProduct(t *testing.T) {
	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	dao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}

	if p.Id != "" {
		t.Error("Product Id must be 0")
	}

	dao.Create(&p)

	if p.Id == "" {
		t.Error("Product Id must be a uuid")
	}

	productRecords := []*ProductRecord{}
	productsBytes, _ := afero.ReadFile(daoFactory.Fs, GetProductsFileName())
	if err := gocsv.UnmarshalBytes(productsBytes, &productRecords); err != nil { // Load clients from file
		log.Fatal(err)
	}

	find := false
	for _, productRecord := range productRecords {
		if productRecord.Id == p.Id {
			find = true
		}
	}

	if !find {
		t.Error("Product not created")
	}
}

func TestGetProduct(t *testing.T) {
	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	dao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	dao.Create(&p)

	obj, err := dao.Get(p.Id)

	if err != nil {
		t.Error(err)
	}

	if (*obj).Id != p.Id {
		t.Errorf("want(Id : %s) \nhave(Id: %s)", p.Id, (*obj).Id)
	}
}

func TestUpdateProduct(t *testing.T) {
	// create an product
	// get an id
	// modify this product
	// verify that the corresponding item of the id has been modified

	newName := "new name"
	newDescription := "new description"

	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	dao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	dao.Create(&p)

	p.Name = newName
	p.Description = newDescription

	err := dao.Update(&p)

	if err != nil {
		t.Error(err)
	}

	// verify in database

	obj, err := dao.Get(p.Id)

	if (*obj).Name != p.Name {
		t.Errorf("want(name : %s) \nhave(name: %s)", p.Name, (*obj).Name)
	}

	if (*obj).Description != p.Description {
		t.Errorf("want(description : %s) \nhave(description : %s)", p.Description, (*obj).Description)
	}
}

func TestDeleteProduct(t *testing.T) {
	// create an product
	// get an id
	// delete this
	// verify that the corresponding item of the id has been deleted

	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	dao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	dao.Create(&p)

	// obj is in db
	_, err := dao.Get(p.Id)

	if err != nil {
		t.Error(err)
	}

	errDelete := dao.Delete(p.Id)

	if errDelete != nil {
		t.Error(err)
	}

	// verify is not in db anymore

	obj, err := dao.Get(p.Id)
	if obj != nil {
		t.Error("Obj must be nil")
	}

	if err.Error() != "No match found" {
		t.Error("Error must be : \"No match found\"")
	}
}

func TestGetAllProduct(t *testing.T) {
	// create 2 products
	// get all from db
	// verify we get 2

	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	dao := daoFactory.GetProductRepository()
	p1 := models.Product{Name: "test 1", Description: "description 1"}
	p2 := models.Product{Name: "test 2", Description: "description 2"}
	dao.Create(&p1)
	dao.Create(&p2)

	products, err := dao.GetAll()

	if err != nil {
		t.Error(err)
	}

	if len(products) != 2 {
		t.Errorf("want 2 products, got %d", len(products))
	}

	errDelete := dao.Delete(p1.Id)

	if errDelete != nil {
		t.Error(err)
	}

	products, errGetAll := dao.GetAll()

	if errGetAll != nil {
		t.Error(err)
	}

	if len(products) != 1 {
		t.Errorf("want 1 products, got %d", len(products))
	}

}
