package csv

import (
	"bakery-manager/bakery/core/models"
)

type RecipeRepository struct {
}

func (r RecipeRepository) Get(id string) (*models.Recipe, error) {
	var obj models.Recipe
	return &obj, nil
}

func (r RecipeRepository) Create(product *models.Recipe) error {
	return nil
}

func (r RecipeRepository) Update(product *models.Recipe) error {
	return nil
}

func (r RecipeRepository) Delete(id string) error {
	return nil
}

func (r RecipeRepository) GetAll() ([]*models.Recipe, error) {
	var recipes []*models.Recipe
	return recipes, nil
}
