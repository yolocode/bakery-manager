package csv

import (
	"bakery-manager/bakery/core/models"
	"log"
	"testing"

	"github.com/gocarina/gocsv"
	"github.com/spf13/afero"
)

func TestCreateIngredient(t *testing.T) {
	fs := afero.NewMemMapFs()
	// init daos
	daoFactory := CsvDaoFactory{Fs: fs}
	productDao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	productDao.Create(&p)
	dao := daoFactory.GetIngredientRepository()

	i := models.Ingredient{
		Product:  p,
		Quantity: 12,
		Unity:    models.IngredientUnityG,
	}

	if i.Id != "" {
		t.Error("Ingredient Id must be 0")
	}

	dao.Create(&i)

	if i.Id == "" {
		t.Error("Ingredient Id must be a uuid")
	}

	ingredientRecords := []*IngredientRecord{}
	ingredientsBytes, _ := afero.ReadFile(daoFactory.Fs, GetIngredientsFileName())
	if err := gocsv.UnmarshalBytes(ingredientsBytes, &ingredientRecords); err != nil { // Load clients from file
		log.Fatal(err)
	}

	find := false
	for _, ingredientRecord := range ingredientRecords {
		if ingredientRecord.Id == i.Id {
			find = true
		}
	}

	if !find {
		t.Error("Ingredient not created")
	}
}

func TestGetIngredient(t *testing.T) {
	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	productDao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	productDao.Create(&p)
	dao := daoFactory.GetIngredientRepository()
	i := models.Ingredient{
		Product:  p,
		Quantity: 12,
		Unity:    models.IngredientUnityG,
	}
	dao.Create(&i)

	obj, err := dao.Get(i.Id)

	if err != nil {
		t.Error(err)
	}

	if (*obj).Id != i.Id {
		t.Errorf("want(Id : %s) \nhave(Id: %s)", i.Id, (*obj).Id)
	}
}

func TestUpdateIngredient(t *testing.T) {
	// create an product
	// get an id
	// modify this product
	// verify that the corresponding item of the id has been modified

	newQuantity := 15
	newUnity := models.IngredientUnityMG

	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	productDao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	productDao.Create(&p)
	dao := daoFactory.GetIngredientRepository()
	i := models.Ingredient{
		Product:  p,
		Quantity: 12,
		Unity:    models.IngredientUnityG,
	}
	dao.Create(&i)

	i.Quantity = newQuantity
	i.Unity = newUnity

	err := dao.Update(&i)

	if err != nil {
		t.Error(err)
	}

	// verify in database

	obj, err := dao.Get(i.Id)

	if (*obj).Quantity != i.Quantity {
		t.Errorf("want(quantity : %d) \nhave(quantity: %d)", i.Quantity, (*obj).Quantity)
	}

	if (*obj).Unity != i.Unity {
		t.Errorf("want(description : %s) \nhave(description : %s)", i.Unity, (*obj).Unity)
	}
}

func TestDeleteIngredient(t *testing.T) {
	// create an product
	// get an id
	// delete this
	// verify that the corresponding item of the id has been deleted

	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	productDao := daoFactory.GetProductRepository()
	p := models.Product{Name: "test 1", Description: "description"}
	productDao.Create(&p)
	dao := daoFactory.GetIngredientRepository()
	i := models.Ingredient{
		Product:  p,
		Quantity: 12,
		Unity:    models.IngredientUnityG,
	}
	dao.Create(&i)

	// obj is in db
	_, err := dao.Get(i.Id)

	if err != nil {
		t.Error(err)
	}

	errDelete := dao.Delete(i.Id)

	if errDelete != nil {
		t.Error(err)
	}

	// verify is not in db anymore

	obj, err := dao.Get(i.Id)
	if obj != nil {
		t.Error("Obj must be nil")
	}

	if err.Error() != "No match found" {
		t.Error("Error must be : \"No match found\"")
	}
}

func TestGetAllIngredients(t *testing.T) {
	// create 2 products
	// get all from db
	// verify we get 2

	fs := afero.NewMemMapFs()
	// init dao
	daoFactory := CsvDaoFactory{Fs: fs}
	productDao := daoFactory.GetProductRepository()
	p1 := models.Product{Name: "test 1", Description: "description 1"}
	p2 := models.Product{Name: "test 2", Description: "description 2"}
	productDao.Create(&p1)
	productDao.Create(&p2)
	dao := daoFactory.GetIngredientRepository()
	i1 := models.Ingredient{
		Product:  p1,
		Quantity: 12,
		Unity:    models.IngredientUnityG,
	}
	i2 := models.Ingredient{
		Product:  p2,
		Quantity: 20,
		Unity:    models.IngredientUnityMG,
	}
	dao.Create(&i1)
	dao.Create(&i2)

	products, err := dao.GetAll()

	if err != nil {
		t.Error(err)
	}

	if len(products) != 2 {
		t.Errorf("want 2 products, got %d", len(products))
	}

	errDelete := dao.Delete(i1.Id)

	if errDelete != nil {
		t.Error(err)
	}

	products, errGetAll := dao.GetAll()

	if errGetAll != nil {
		t.Error(err)
	}

	if len(products) != 1 {
		t.Errorf("want 1 products, got %d", len(products))
	}

}
