package csv

import (
	"bakery-manager/bakery/core/models"
)

type SessionRepository struct {
}

func (s SessionRepository) Get(id string) (*models.Session, error) {
	var obj models.Session
	return &obj, nil
}

func (s SessionRepository) Create(product *models.Session) error {
	return nil
}

func (s SessionRepository) Update(product *models.Session) error {
	return nil
}

func (s SessionRepository) Delete(id string) error {
	return nil
}

func (s SessionRepository) GetAll() ([]*models.Session, error) {
	var sessions []*models.Session
	return sessions, nil
}
