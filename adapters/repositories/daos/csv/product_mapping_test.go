package csv

import (
	"bakery-manager/bakery/core/models"
	"testing"
)

func TestMapFromProduct(t *testing.T) {
	id := "0"
	name := "nom"
	description := "description"

	p := models.Product{Id: id, Name: name, Description: description}
	pm := MapProduct(p)

	if pm.Id != id {
		t.Errorf("Id = %s; want %s", pm.Id, p.Id)
	}
	if pm.Name != name {
		t.Errorf("name = \"%s\"; want \"%s\"", pm.Name, p.Name)
	}
	if pm.Description != description {
		t.Errorf("Description = \"%s\"; want \"%s\"", pm.Description, p.Description)
	}
}

func TestMappingToProduct(t *testing.T) {
	id := "1"
	name := "nom"
	description := "description"

	pm := ProductRecord{id, name, description}
	p := pm.ToModel()

	if p.Id != id {
		t.Errorf("Id = %s; want %s", p.Id, id)
	}
	if p.Name != name {
		t.Errorf("name = \"%s\"; want \"%s\"", p.Name, name)
	}
	if p.Description != description {
		t.Errorf("Description = \"%s\"; want \"%s\"", p.Description, description)
	}
}
