package csv

import (
	"bakery-manager/bakery/core/models"
)

type ProductRecord struct {
	Id          string `csv:"id"`
	Name        string `csv:"name"`
	Description string `csv:"description"`
}

func (pr ProductRecord) ToModel() models.Product {
	var obj models.Product
	obj.Init(pr.Name, pr.Description)
	obj.Id = pr.Id
	return obj
}

func MapProduct(p models.Product) ProductRecord {
	return ProductRecord{Id: p.Id, Name: p.Name, Description: p.Description}
}
