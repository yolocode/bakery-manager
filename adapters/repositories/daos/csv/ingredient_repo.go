package csv

import (
	"bakery-manager/bakery/core/models"
	"errors"
	"log"
	"os"

	"github.com/gocarina/gocsv"
	"github.com/google/uuid"
	"github.com/spf13/afero"
)

type IngredientRepository struct {
	Fs afero.Fs
}

func GetIngredientsFileName() string {
	return GetFileName("ingredients")
}

func (p IngredientRepository) GetFs() afero.Fs {
	return p.Fs
}

func (i IngredientRepository) Get(id string) (*models.Ingredient, error) {
	// load foreign key repos
	daoFactory := CsvDaoFactory{Fs: i.GetFs()}
	productDao := daoFactory.GetProductRepository()

	ingredientsBytes, errRF := afero.ReadFile(i.GetFs(), GetIngredientsFileName())
	if errRF != nil {
		log.Fatal(errRF)
	}

	ingredientRecords := []*IngredientRecord{}
	if err := gocsv.UnmarshalBytes(ingredientsBytes, &ingredientRecords); err != nil { // Load clients from file
		if err.Error() != "empty csv file given" {
			log.Print(err)
		}
	}

	for _, ingredientRecord := range ingredientRecords {
		if ingredientRecord.Id == id {
			// load foreign key
			product, err := productDao.Get(ingredientRecord.ProductId)
			if err != nil {
				return nil, err
			}
			ingredientToReturn := ingredientRecord.ToModel((*product))

			return &ingredientToReturn, nil
		}
	}
	return nil, errors.New("No match found")
}

func (i IngredientRepository) Create(ingredient *models.Ingredient) error {
	ingredient.Id = uuid.New().String()

	// load ingredientRecords
	ingredientRecords := []*IngredientRecord{}

	ingredientsBytes, errRF := afero.ReadFile(i.GetFs(), GetIngredientsFileName())
	if errRF != nil {
		log.Fatal(errRF)
	}

	if err := gocsv.UnmarshalBytes(ingredientsBytes, &ingredientRecords); err != nil { // Load clients from file
		if err.Error() != "empty csv file given" {
			log.Print(err)
		}
	}

	mappedIngredient := MapIngredient((*ingredient))

	//append object
	ingredientRecords = append(ingredientRecords, &mappedIngredient)
	bytesToWrite, errMB := gocsv.MarshalBytes(&ingredientRecords)
	if errMB != nil {
		log.Fatalf("Can't marshal bytes : %s", errMB)
	}

	errWF := afero.WriteFile(i.GetFs(), GetIngredientsFileName(), bytesToWrite, os.ModePerm)
	if errWF != nil {
		log.Fatal(errWF)
	}
	return nil
}

func (i IngredientRepository) Update(ingredient *models.Ingredient) error {
	// load ingredientRecords
	ingredientRecords := []*IngredientRecord{}

	ingredientsBytes, errRF := afero.ReadFile(i.GetFs(), GetIngredientsFileName())
	if errRF != nil {
		return errRF
	}

	if err := gocsv.UnmarshalBytes(ingredientsBytes, &ingredientRecords); err != nil { // Load clients from file
		return err
	}

	for idx, ingredientRecord := range ingredientRecords {
		if ingredientRecord.Id == (*ingredient).Id {
			mappedIngredient := MapIngredient((*ingredient))
			ingredientRecords[idx] = &mappedIngredient
		}
	}

	bytesToWrite, errMB := gocsv.MarshalBytes(&ingredientRecords)
	if errMB != nil {
		log.Fatal(errMB)
	}

	errWF := afero.WriteFile(i.GetFs(), GetIngredientsFileName(), bytesToWrite, os.ModePerm)
	if errWF != nil {
		log.Fatal(errWF)
	}
	return nil
}

func (i IngredientRepository) Delete(id string) error {
	// load ingredientRecords
	ingredientRecords := []*IngredientRecord{}

	ingredientsBytes, errRF := afero.ReadFile(i.GetFs(), GetIngredientsFileName())
	if errRF != nil {
		return errRF
	}

	if err := gocsv.UnmarshalBytes(ingredientsBytes, &ingredientRecords); err != nil { // Load clients from file
		return err
	}

	idxToDel := -1

	for idx, ingredientRecord := range ingredientRecords {
		if ingredientRecord.Id == id {
			idxToDel = idx
		}
	}

	if idxToDel < 0 {
		return errors.New("No match found to delete")
	}
	ingredientRecords = append(ingredientRecords[:idxToDel], ingredientRecords[(idxToDel+1):]...)

	bytesToWrite, errMB := gocsv.MarshalBytes(&ingredientRecords)
	if errMB != nil {
		log.Fatal(errMB)
	}

	errWF := afero.WriteFile(i.GetFs(), GetIngredientsFileName(), bytesToWrite, os.ModePerm)
	if errWF != nil {
		log.Fatal(errWF)
	}

	return nil
}

func (i IngredientRepository) GetAll() ([]*models.Ingredient, error) {
	// load foreign key repos
	daoFactory := CsvDaoFactory{Fs: i.GetFs()}
	productDao := daoFactory.GetProductRepository()

	ingredients := []*models.Ingredient{}

	ingredientsBytes, errRF := afero.ReadFile(i.GetFs(), GetIngredientsFileName())
	if errRF != nil {
		return nil, errRF
	}

	ingredientRecords := []*IngredientRecord{}
	if err := gocsv.UnmarshalBytes(ingredientsBytes, &ingredientRecords); err != nil { // Load clients from file
		return nil, err
	}
	products, err := productDao.GetAll()

	if err != nil {
		return nil, err
	}

	for _, ingredientRecord := range ingredientRecords {
		var productToLink models.Product
		for _, product := range products {
			if product.Id == ingredientRecord.ProductId {
				productToLink = (*product)
				break
			}
		}
		ingredient := ingredientRecord.ToModel(productToLink)
		ingredients = append(ingredients, &ingredient) // Add clients
	}

	return ingredients, nil
}
