package csv

import (
	"bakery-manager/bakery/core/models"
	"errors"
	"log"
	"os"

	"github.com/gocarina/gocsv"
	"github.com/google/uuid"
	"github.com/spf13/afero"
)

type ProductRepository struct {
	Fs afero.Fs
}

func GetProductsFileName() string {
	return GetFileName("products")
}

func (p ProductRepository) GetFs() afero.Fs {
	return p.Fs
}

func (p ProductRepository) Get(id string) (*models.Product, error) {
	productsBytes, errRF := afero.ReadFile(p.GetFs(), GetProductsFileName())
	if errRF != nil {
		log.Fatal(errRF)
	}

	productRecords := []*ProductRecord{}
	if err := gocsv.UnmarshalBytes(productsBytes, &productRecords); err != nil {
		if err.Error() != "empty csv file given" {
			log.Print(err)
		}
	}

	for _, productRecord := range productRecords {
		if productRecord.Id == id {
			productToReturn := productRecord.ToModel()
			return &productToReturn, nil
		}
	}
	return nil, errors.New("No match found")
}

func (p ProductRepository) Create(product *models.Product) error {
	product.Id = uuid.New().String()

	// load productRecords
	productRecords := []*ProductRecord{}

	productsBytes, errRF := afero.ReadFile(p.GetFs(), GetProductsFileName())
	if errRF != nil {
		log.Fatal(errRF)
	}

	if err := gocsv.UnmarshalBytes(productsBytes, &productRecords); err != nil {
		if err.Error() != "empty csv file given" {
			log.Print(err)
		}
	}

	mappedProduct := MapProduct((*product))

	//append object
	productRecords = append(productRecords, &mappedProduct)
	bytesToWrite, errMB := gocsv.MarshalBytes(&productRecords)
	if errMB != nil {
		log.Fatal(errMB)
	}

	errWF := afero.WriteFile(p.GetFs(), GetProductsFileName(), bytesToWrite, os.ModePerm)
	if errWF != nil {
		log.Fatal(errWF)
	}
	return nil
}

func (p ProductRepository) Update(product *models.Product) error {
	// load productRecords
	productRecords := []*ProductRecord{}

	productsBytes, errRF := afero.ReadFile(p.GetFs(), GetProductsFileName())
	if errRF != nil {
		return errRF
	}

	if err := gocsv.UnmarshalBytes(productsBytes, &productRecords); err != nil {
		return err
	}

	for idx, productRecord := range productRecords {
		if productRecord.Id == (*product).Id {
			mappedProduct := MapProduct((*product))
			productRecords[idx] = &mappedProduct
		}
	}

	bytesToWrite, errMB := gocsv.MarshalBytes(&productRecords)
	if errMB != nil {
		log.Fatal(errMB)
	}

	errWF := afero.WriteFile(p.GetFs(), GetProductsFileName(), bytesToWrite, os.ModePerm)
	if errWF != nil {
		log.Fatal(errWF)
	}

	return nil
}

func (p ProductRepository) Delete(id string) error {
	// load productRecords
	productRecords := []*ProductRecord{}

	productsBytes, errRF := afero.ReadFile(p.GetFs(), GetProductsFileName())
	if errRF != nil {
		return errRF
	}

	if err := gocsv.UnmarshalBytes(productsBytes, &productRecords); err != nil {
		return err
	}

	idxToDel := -1

	for idx, productRecord := range productRecords {
		if productRecord.Id == id {
			idxToDel = idx
		}
	}

	if idxToDel < 0 {
		return errors.New("No match found to delete")
	}
	productRecords = append(productRecords[:idxToDel], productRecords[(idxToDel+1):]...)

	bytesToWrite, errMB := gocsv.MarshalBytes(&productRecords)
	if errMB != nil {
		log.Fatal(errMB)
	}

	errWF := afero.WriteFile(p.GetFs(), GetProductsFileName(), bytesToWrite, os.ModePerm)
	if errWF != nil {
		log.Fatal(errWF)
	}

	return nil
}

func (p ProductRepository) GetAll() ([]*models.Product, error) {
	products := []*models.Product{}

	productsBytes, errRF := afero.ReadFile(p.GetFs(), GetProductsFileName())
	if errRF != nil {
		return nil, errRF
	}

	productRecords := []*ProductRecord{}
	if err := gocsv.UnmarshalBytes(productsBytes, &productRecords); err != nil {
		return nil, err
	}

	for _, productRecord := range productRecords {
		product := productRecord.ToModel()
		products = append(products, &product)
	}

	return products, nil
}
