package csv

import (
	"bakery-manager/bakery/core/models"
)

type IngredientRecord struct {
	Id        string `csv:"id"`
	Quantity  int    `csv:"quantity"`
	ProductId string `csv:"product_id"`
	Unity     string `csv:"unity"`
}

func (ir IngredientRecord) ToModel(product models.Product) models.Ingredient {
	var obj models.Ingredient
	// retrieve foreign key only when needed
	obj.Init(ir.Quantity, models.IngredientUnityEnum(ir.Unity), product)
	obj.Id = ir.Id
	return obj
}

func MapIngredient(i models.Ingredient) IngredientRecord {
	return IngredientRecord{
		Id:        i.Id,
		Quantity:  i.Quantity,
		ProductId: i.Product.Id,
		Unity:     string(i.Unity),
	}
}
