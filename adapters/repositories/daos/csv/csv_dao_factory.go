package csv

import (
	"bakery-manager/bakery/core/ports"
	"log"
	"os"

	"github.com/spf13/afero"
)

type CsvDaoFactory struct {
	Fs afero.Fs
}

func GetFileName(entity string) string {
	filePrefix := os.Getenv("CSV_FILEPATH")
	return filePrefix + "/" + entity + ".csv"
}

func (c CsvDaoFactory) GetProductRepository() ports.ProductRepository {
	_, err := c.Fs.OpenFile(GetFileName("products"), os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		log.Fatalf("GetProductRepository : %s", err.Error())
	}
	return ProductRepository{Fs: c.Fs}
}

func (c CsvDaoFactory) GetIngredientRepository() ports.IngredientRepository {
	_, err := c.Fs.OpenFile(GetFileName("ingredients"), os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		log.Fatalf("GetIngredientRepository : %s", err.Error())
	}
	return IngredientRepository{Fs: c.Fs}
}

func (c CsvDaoFactory) GetRecipeRepository() ports.RecipeRepository {
	return RecipeRepository{}
}

func (c CsvDaoFactory) GetSessionRepository() ports.SessionRepository {
	return SessionRepository{}
}

func (c CsvDaoFactory) GetStepRepository() ports.StepRepository {
	return StepRepository{}
}
