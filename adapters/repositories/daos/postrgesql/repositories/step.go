package repositories

import (
	"bakery-manager/adapters/repositories/daos/postrgesql/mapping"
	"bakery-manager/bakery/core/models"

	"gorm.io/gorm"
)

type StepRepository struct {
	db *gorm.DB
}

func (s StepRepository) Get(id string) (*models.Step, error) {
	var stepRecord mapping.StepRecord
	result := s.db.First(&stepRecord, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}
	step := stepRecord.ToModel()
	return &step, nil
}

func (s StepRepository) Create(step *models.Step) error {
	mappedStep := mapping.MapStep(*step)
	result := s.db.Create(&mappedStep)
	if result.Error == nil {
		step.Id = mappedStep.ID
	}
	return result.Error
}

func (s StepRepository) Update(step *models.Step) error {
	mappedStep := mapping.MapStep(*step)
	result := s.db.Save(&mappedStep)
	return result.Error
}

func (s StepRepository) Delete(id string) error {
	result := s.db.Delete(&mapping.StepRecord{}, "id = ?", id)
	return result.Error
}

func (s StepRepository) GetAll() ([]*models.Step, error) {
	var stepRecords []mapping.StepRecord
	steps := []*models.Step{}

	result := s.db.Find(&stepRecords)
	if result.Error != nil {
		return nil, result.Error
	}

	for _, stepRecord := range stepRecords {
		step := stepRecord.ToModel()
		steps = append(steps, &step)
	}

	return steps, nil
}
