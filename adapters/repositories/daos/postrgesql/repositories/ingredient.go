package repositories

import (
	"bakery-manager/adapters/repositories/daos/postrgesql/mapping"
	"bakery-manager/bakery/core/models"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type IngredientRepository struct {
	db *gorm.DB
}

func (ir IngredientRepository) Get(id string) (*models.Ingredient, error) {
	var ingredientRecord mapping.IngredientRecord
	result := ir.db.Joins("Product").First(&ingredientRecord, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}
	ingredient := ingredientRecord.ToModel()
	return &ingredient, nil
}

func (ir IngredientRepository) Create(ingredient *models.Ingredient) error {
	mappedIngredient := mapping.MapIngredient((*ingredient))
	mappedIngredient.ID = uuid.New().String()
	result := ir.db.Create(&mappedIngredient)
	if result.Error == nil {
		ingredient.Id = mappedIngredient.ID
	}
	return result.Error
}

func (ir IngredientRepository) Update(ingredient *models.Ingredient) error {
	mappedIngredient := mapping.MapIngredient((*ingredient))
	result := ir.db.Save(&mappedIngredient)
	return result.Error
}

func (ir IngredientRepository) Delete(id string) error {
	result := ir.db.Delete(&mapping.IngredientRecord{}, id)
	return result.Error
}

func (ir IngredientRepository) GetAll() ([]*models.Ingredient, error) {
	ingredientRecords := []mapping.IngredientRecord{}
	ingredients := []*models.Ingredient{}
	ir.db.Joins("Product").Find(&ingredientRecords)
	for _, ingredientRecord := range ingredientRecords {
		ingredient := ingredientRecord.ToModel()
		ingredients = append(ingredients, &ingredient) // Add product
	}
	return ingredients, nil
}
