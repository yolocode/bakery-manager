package repositories

import (
	"bakery-manager/adapters/repositories/daos/postrgesql/mapping"
	"bakery-manager/bakery/core/models"

	"gorm.io/gorm"
)

type SessionRepository struct {
	db *gorm.DB
}

func (s SessionRepository) Get(id string) (*models.Session, error) {
	var sessionRecord mapping.SessionRecord
	result := s.db.Preload("Recipes").First(&sessionRecord, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}
	session := sessionRecord.ToModel()
	return &session, nil
}

func (s SessionRepository) Create(session *models.Session) error {
	mappedSession := mapping.MapSession(*session)
	result := s.db.Create(&mappedSession)
	if result.Error == nil {
		session.Id = mappedSession.ID
	}
	return result.Error
}

func (s SessionRepository) Update(session *models.Session) error {
	mappedSession := mapping.MapSession(*session)
	result := s.db.Save(&mappedSession)
	return result.Error
}

func (s SessionRepository) Delete(id string) error {
	result := s.db.Delete(&mapping.SessionRecord{}, "id = ?", id)
	return result.Error
}

func (s SessionRepository) GetAll() ([]*models.Session, error) {
	var sessionRecords []mapping.SessionRecord
	sessions := []*models.Session{}

	result := s.db.Preload("Recipes").Find(&sessionRecords)
	if result.Error != nil {
		return nil, result.Error
	}

	for _, sessionRecord := range sessionRecords {
		session := sessionRecord.ToModel()
		sessions = append(sessions, &session)
	}

	return sessions, nil
}
