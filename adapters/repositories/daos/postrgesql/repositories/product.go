package repositories

import (
	"bakery-manager/adapters/repositories/daos/postrgesql/mapping"
	"bakery-manager/bakery/core/models"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ProductRepository struct {
	db *gorm.DB
}

func (p ProductRepository) Get(id string) (*models.Product, error) {
	var productRecord mapping.ProductRecord
	result := p.db.First(&productRecord, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}
	product := productRecord.ToModel()
	return &product, nil
}

func (p ProductRepository) Create(product *models.Product) error {
	mappedProduct := mapping.MapProduct((*product))
	mappedProduct.ID = uuid.New().String()
	result := p.db.Create(&mappedProduct)
	if result.Error == nil {
		product.Id = mappedProduct.ID
	}
	return result.Error
}

func (p ProductRepository) Update(product *models.Product) error {
	mappedProduct := mapping.MapProduct((*product))
	result := p.db.Save(&mappedProduct)
	return result.Error
}

func (p ProductRepository) Delete(id string) error {
	p.db.Delete(&mapping.ProductRecord{}, id)
	return nil
}

func (p ProductRepository) GetAll() ([]*models.Product, error) {
	productRecords := []mapping.ProductRecord{}
	products := []*models.Product{}
	p.db.Find(&productRecords)
	for _, productRecord := range productRecords {
		product := productRecord.ToModel()
		products = append(products, &product) // Add product
	}
	return products, nil
}
