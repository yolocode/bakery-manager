package repositories

import (
	"bakery-manager/adapters/repositories/daos/postrgesql/mapping"
	"bakery-manager/bakery/core/models"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type RecipeRepository struct {
	db *gorm.DB
}

func (r RecipeRepository) Get(id string) (*models.Recipe, error) {
	var recipeRecord mapping.RecipeRecord
	result := r.db.First(&recipeRecord, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}
	recipe := recipeRecord.ToModel()
	return &recipe, nil
}

func (r RecipeRepository) Create(recipe *models.Recipe) error {
	mappedRecipe := mapping.MapRecipe((*recipe))
	mappedRecipe.ID = uuid.New().String()
	result := r.db.Create(&mappedRecipe)
	if result.Error == nil {
		recipe.Id = mappedRecipe.ID
	}
	return result.Error
}

func (r RecipeRepository) Update(recipe *models.Recipe) error {
	mappedRecipe := mapping.MapRecipe((*recipe))
	result := r.db.Save(&mappedRecipe)
	return result.Error
}

func (r RecipeRepository) Delete(id string) error {
	r.db.Delete(&mapping.RecipeRecord{}, id)
	return nil
}

func (r RecipeRepository) GetAll() ([]*models.Recipe, error) {
	recipeRecords := []mapping.RecipeRecord{}
	recipes := []*models.Recipe{}
	r.db.Find(&recipeRecords)
	for _, recipeRecord := range recipeRecords {
		recipe := recipeRecord.ToModel()
		recipes = append(recipes, &recipe) // Add recipe
	}
	return recipes, nil
}
