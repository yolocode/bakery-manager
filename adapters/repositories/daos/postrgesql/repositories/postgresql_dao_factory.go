package repositories

import (
	"bakery-manager/bakery/core/ports"
)

type PostgresqlDaoFactory struct {
}

func (p PostgresqlDaoFactory) GetProductRepository() ports.ProductRepository {
	postgres := GetInstance()
	return ProductRepository{
		db: postgres.db,
	}
}

func (p PostgresqlDaoFactory) GetIngredientRepository() ports.IngredientRepository {
	postgres := GetInstance()
	return IngredientRepository{
		db: postgres.db,
	}
}

func (p PostgresqlDaoFactory) GetRecipeRepository() ports.RecipeRepository {
	postgres := GetInstance()
	return RecipeRepository{
		db: postgres.db,
	}
}

func (p PostgresqlDaoFactory) GetSessionRepository() ports.SessionRepository {
	postgres := GetInstance()
	return SessionRepository{db: postgres.db}
}

func (p PostgresqlDaoFactory) GetStepRepository() ports.StepRepository {
	postgres := GetInstance()
	return StepRepository{
		db: postgres.db,
	}
}
