package repositories

import (
	"bakery-manager/adapters/repositories/daos/postrgesql/mapping"
	"fmt"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// instance represents the unique instance of DbConnexion
var connexion *DatabaseConnexion

type DatabaseConnexion struct {
	db *gorm.DB
}

func (m *DatabaseConnexion) Init() (err error) {
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASSWORD")
	dbname := os.Getenv("POSTGRES_DATABASE_NAME")
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai", host, user, password, dbname, port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	m.db = db

	err = db.AutoMigrate(
		&mapping.IngredientRecord{},
		&mapping.ProductRecord{},
		&mapping.RecipeRecord{},
		&mapping.SessionRecord{},
		&mapping.StepRecord{},
	)

	if err != nil {
		log.Fatal(err)
	}

	log.Print("Database connexion initialized")
	return err
}

// GetInstance returns the unique instance of a repository manager
func GetInstance() *DatabaseConnexion {
	if connexion == nil {
		connexion = newDatabaseConnexion()
	}
	return connexion
}

func newDatabaseConnexion() *DatabaseConnexion {
	return &DatabaseConnexion{}
}
