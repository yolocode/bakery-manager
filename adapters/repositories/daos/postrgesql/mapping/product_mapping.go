package mapping

import (
	"bakery-manager/bakery/core/models"
)

type ProductRecord struct {
	Model
	Name        string
	Description string
}

func (pr ProductRecord) ToModel() models.Product {
	var obj models.Product
	obj.Init(pr.Name, pr.Description)
	obj.Id = pr.ID
	return obj
}

func MapProduct(p models.Product) ProductRecord {
	pr := ProductRecord{Name: p.Name, Description: p.Description}
	pr.ID = p.Id

	return pr
}
