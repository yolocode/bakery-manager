package mapping

import (
	"bakery-manager/bakery/core/models"
)

type RecipeRecord struct {
	Model
	Name            string
	Description     string
	Steps           *[]*StepRecord `gorm:"foreignKey:RecipeID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	SessionRecordID string
	Sessions        []*SessionRecord `gorm:"many2many:session_recipes;"`
}

func (rr RecipeRecord) ToModel() models.Recipe {
	var obj models.Recipe
	// Map fields from RecipeRecord to models.Recipe
	obj.Id = rr.ID
	obj.Name = rr.Name
	obj.Description = rr.Description
	// Assuming Steps is a slice of Step models in models.Recipe
	for _, stepRecord := range *rr.Steps {
		step := stepRecord.ToModel() // Assuming you have a ToModel method for StepRecord
		obj.Steps = append(obj.Steps, step)
	}
	// Map other fields as necessary
	return obj
}

func MapRecipe(r models.Recipe) RecipeRecord {
	var rr RecipeRecord
	// Map fields from models.Recipe to RecipeRecord
	rr.ID = r.Id
	rr.Name = r.Name
	rr.Description = r.Description

	stepRecords := []*StepRecord{}
	for _, step := range r.Steps {
		stepRecord := MapStep(step) // Assuming you have a MapRecipe function for models.Recipe
		stepRecords = append(stepRecords, &stepRecord)
	}
	rr.Steps = &stepRecords // Set the pointer to the slice
	// Map other fields as necessary
	return rr
}
