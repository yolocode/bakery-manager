package mapping

import (
	"bakery-manager/bakery/core/models"
)

type StepRecord struct {
	Model
	Name        string
	Description string
	Duration    int
	Rank        int
	Recipe      *RecipeRecord
	RecipeID    string `gorm:"index;"`
}

func (sr StepRecord) ToModel() models.Step {
	var obj models.Step
	// Map fields from StepRecord to models.Step
	obj.Id = sr.ID
	obj.Name = sr.Name
	obj.Description = sr.Description
	obj.Duration = sr.Duration
	obj.Rank = sr.Rank
	// Map other fields as necessary
	return obj
}

func MapStep(s models.Step) StepRecord {
	var sr StepRecord
	// Map fields from models.Step to StepRecord
	sr.ID = s.Id
	sr.Name = s.Name
	sr.Description = s.Description
	sr.Duration = s.Duration
	sr.Rank = s.Rank
	// Map other fields as necessary
	return sr
}
