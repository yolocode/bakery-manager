package mapping

import (
	"bakery-manager/bakery/core/models"
)

type IngredientRecord struct {
	Model
	Quantity  int
	Product   *ProductRecord
	ProductID string `gorm:"not null;"`
	Unity     string `gorm:"not null;"`
}

func (ir IngredientRecord) ToModel() models.Ingredient {
	var obj models.Ingredient
	var product models.Product
	if ir.Product != nil {
		product = ir.Product.ToModel()
	}
	obj.Init(ir.Quantity, models.IngredientUnityEnum(ir.Unity), product)
	obj.Id = ir.ID
	return obj
}

func MapIngredient(i models.Ingredient) IngredientRecord {
	product := MapProduct(i.Product)
	ingredientRecord := IngredientRecord{
		Quantity:  i.Quantity,
		Product:   &product,
		ProductID: i.Product.Id,
		Unity:     string(i.Unity),
	}
	return ingredientRecord
}
