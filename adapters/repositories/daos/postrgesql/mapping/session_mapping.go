package mapping

import (
	"bakery-manager/bakery/core/models"
)

type SessionRecord struct {
	Model
	Recipes *[]*RecipeRecord `gorm:"many2many:session_recipes;"`
}

func (sr SessionRecord) ToModel() models.Session {
	var obj models.Session
	// Map fields from SessionRecord to models.Session
	obj.Id = sr.ID
	// Assuming Recipes is a slice of Recipe models in models.Session
	for _, recipeRecord := range *sr.Recipes {
		recipe := recipeRecord.ToModel() // Assuming you have a ToModel method for RecipeRecord
		obj.Recipes = append(obj.Recipes, recipe)
	}
	// Map other fields as necessary
	return obj
}

func MapSession(s models.Session) SessionRecord {
	var sr SessionRecord
	// Map fields from models.Session to SessionRecord
	sr.ID = s.Id
	recipeRecords := []*RecipeRecord{}
	for _, recipe := range s.Recipes {
		recipeRecord := MapRecipe(recipe) // Assuming you have a MapRecipe function for models.Recipe
		recipeRecords = append(recipeRecords, &recipeRecord)
	}
	sr.Recipes = &recipeRecords // Set the pointer to the slice
	// Map other fields as necessary
	return sr
}
