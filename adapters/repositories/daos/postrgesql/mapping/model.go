package mapping

import (
	"time"

	"gorm.io/gorm"
)

type ModelInterface interface {
	GetID() string
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
}

// Model is the base ORM model
type Model struct {
	ID        string `gorm:"primary_key;size:36"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

func (m Model) GetID() string {
	return m.ID
}

func (m Model) GetCreatedAt() time.Time {
	return m.CreatedAt
}

func (m Model) GetUpdatedAt() time.Time {
	return m.UpdatedAt
}
