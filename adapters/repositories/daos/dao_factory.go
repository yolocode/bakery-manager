package daos

import (
	"bakery-manager/adapters/repositories/daos/csv"
	"bakery-manager/adapters/repositories/daos/json"
	"bakery-manager/adapters/repositories/daos/postrgesql/repositories"
	"bakery-manager/bakery/core/ports"
	"fmt"
	"log"
	"testing"

	"github.com/spf13/afero"
)

type IDAOFactory interface {
	GetProductRepository() ports.ProductRepository
	GetIngredientRepository() ports.IngredientRepository
	GetRecipeRepository() ports.RecipeRepository
	GetSessionRepository() ports.SessionRepository
	GetStepRepository() ports.StepRepository
}

func GetAppFileSystem() afero.Fs {
	if testing.Testing() {
		return afero.NewMemMapFs()
	}
	return afero.NewOsFs()
}

func GetDAOFactory(dao string) (IDAOFactory, error) {
	fs := GetAppFileSystem()
	if dao == "csv" {
		return csv.CsvDaoFactory{Fs: fs}, nil
	}

	if dao == "json" {
		return json.JsonDaoFactory{}, nil
	}

	if dao == "postgresql" {
		postgresqlInstance := repositories.GetInstance()
		err := postgresqlInstance.Init()
		if err != nil {
			log.Fatal("Can't initiate db connexion")
		}
		return repositories.PostgresqlDaoFactory{}, nil
	}

	return nil, fmt.Errorf("Wrong dao type passed")
}
