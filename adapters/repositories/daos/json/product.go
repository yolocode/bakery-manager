package json

import (
	"bakery-manager/bakery/core/models"
)

type ProductRepository struct {
}

func (p ProductRepository) Get(id string) (*models.Product, error) {
	var obj models.Product
	obj.Init("Product", "Product description")
	return &obj, nil
}

func (p ProductRepository) Create(product *models.Product) error {
	return nil
}

func (p ProductRepository) Update(product *models.Product) error {
	return nil
}

func (p ProductRepository) Delete(id string) error {
	return nil
}

func (p ProductRepository) GetAll() ([]*models.Product, error) {
	var products []*models.Product
	return products, nil
}
