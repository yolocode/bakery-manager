package json

import (
	"bakery-manager/bakery/core/models"
)

type StepRepository struct {
}

func (s StepRepository) Get(id string) (*models.Step, error) {
	var obj models.Step
	return &obj, nil
}

func (s StepRepository) Create(product *models.Step) error {
	return nil
}

func (s StepRepository) Update(product *models.Step) error {
	return nil
}

func (s StepRepository) Delete(id string) error {
	return nil
}

func (s StepRepository) GetAll() ([]*models.Step, error) {
	var steps []*models.Step
	return steps, nil
}
