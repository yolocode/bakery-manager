package json

import (
	"bakery-manager/bakery/core/models"
)

type IngredientRepository struct {
}

func (p IngredientRepository) Get(id string) (*models.Ingredient, error) {
	var obj models.Ingredient
	return &obj, nil
}

func (p IngredientRepository) Create(product *models.Ingredient) error {
	return nil
}

func (p IngredientRepository) Update(product *models.Ingredient) error {
	return nil
}

func (p IngredientRepository) Delete(id string) error {
	return nil
}

func (p IngredientRepository) GetAll() ([]*models.Ingredient, error) {
	var ingredients []*models.Ingredient
	return ingredients, nil
}
