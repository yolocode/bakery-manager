package json

import (
	"bakery-manager/bakery/core/ports"
)

type JsonDaoFactory struct {
}

func (j JsonDaoFactory) GetProductRepository() ports.ProductRepository {
	return ProductRepository{}
}

func (j JsonDaoFactory) GetIngredientRepository() ports.IngredientRepository {
	return IngredientRepository{}
}

func (j JsonDaoFactory) GetRecipeRepository() ports.RecipeRepository {
	return RecipeRepository{}
}

func (j JsonDaoFactory) GetSessionRepository() ports.SessionRepository {
	return SessionRepository{}
}

func (j JsonDaoFactory) GetStepRepository() ports.StepRepository {
	return StepRepository{}
}
